<%--
  Created by IntelliJ IDEA.
  User: Andrew Meads
  Date: 13/05/2018
  Time: 3:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Web Lab 17</title>
    </head>
    <body>
        <ul>
            <li><a href="ImageGalleryDisplay">Image Gallery</a></li>
            <li><a href="exercise01-01-.jsp">Ex1 01</a></li>
            <li><a href="exercise01-02-.jsp">Ex1 02</a></li>
            <li><a href="exercise02-01.jsp">Ex2 02</a></li>
            <li><a href="exercise02-02.jsp">Ex2 02</a></li>
            <li><a href="Lab%202/edmund_hillary.html">Edmund Hillary</a></li>
            <li><a href="Lab%202/potash.html">potash</a></li>
            <li><a href="Lab%202/potassium.html">potassium</a></li>
            <li><a href="SurveyJSP.jsp">Survey</a></li>
        </ul>
    </body>
</html>
